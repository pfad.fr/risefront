module code.pfad.fr/risefront

go 1.18

require (
	github.com/Microsoft/go-winio v0.5.2
	gotest.tools/v3 v3.3.0
)

require (
	github.com/google/go-cmp v0.5.5 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
